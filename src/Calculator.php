<?php declare (strict_types=1);
/**
 * @author      Jan Mikes <mikes@v-i-c.eu>
 * @copyright   Velveth International Corporation <v-i-c.eu>
 */

namespace Workshop;


class Calculator
{
	public function add(int $x, int $y): int
	{
		return $x + $y;
	}
}